'''Contains a reader, which can read a tree stored in an arbitrary storage
format by using an appropriate parser.
'''
import xml.etree.ElementTree as ET
from structureddata.common import StructuredDataError
from structureddata.msdparser import MsdParser


class TreeReader:
    '''Implements a reader for reading trees from file.

    Args:
        parser (obj): Parser to use. Default: MsdParser()
    '''

    def __init__(self, parser=None):
        if parser is None:
            self._parser = MsdParser()
        else:
            self._parser = parser
        self._parser.open_block = self._open_block
        self._parser.close_block = self._close_block
        self._parser.add_data = self._add_data
        self._root = None
        self._path = None
        self._current = None


    def read(self, fobj):
        '''Reads the tree from a file.

        Args:
            fobj (file object): Open file to read from.

        Returns:
            etree: Element tree representation of the file content.
        '''
        self._root = ET.Element('msd')
        self._root.text = None
        self._path = [self._root]
        self._current = self._root
        self._parser.parse(fobj)
        return self._root


    def _open_block(self, tagname):
        elem = ET.Element(tagname)
        elem.text = None
        self._current.append(elem)
        self._path.append(elem)
        self._current = elem


    def _close_block(self, tagname):
        if tagname != self._current.tag:
            msg = 'Mismatching closing tag: expected {} got {} instead'.format(
                self._current.tag, tagname)
            raise StructuredDataError(msg)
        del self._path[-1]
        self._current = self._path[-1]


    def _add_data(self, tagname, data):
        element = ET.Element(tagname)
        element.text = data
        self._current.append(element)
