'''Contains a writer, which can write a tree to arbitary storage format by using
an appropriate formatter.
'''
from structureddata.msdformatter import MsdFormatter


class TreeWriter:
    '''Implements a writer for writing trees to file.

    Args:
        formatter (obj): Formatter to use. Default: MsdFormatter()
    '''

    def __init__(self, formatter=None):
        if formatter is None:
            self._formatter = MsdFormatter()


    def write(self, fobj, tree, includeroot=False):
        '''Writes a tree to a given file.

        Args:
            fobj (file object): Open file to write to.
            tree (etree): Tree to write.
            includeroot: Whether the top element should be included when writing
                to the file. Default: False.
        '''
        if includeroot:
            self._write(fobj, tree)
        else:
            for child in tree:
                self._write(fobj, child)


    def _write(self, fobj, node):
        if node.text is None:
            fobj.write(self._formatter.open_block(node.tag))
            for child in node:
                self._write(fobj, child)
            fobj.write(self._formatter.close_block(node.tag))
        else:
            fobj.write(self._formatter.add_data(node.tag, node.text))
