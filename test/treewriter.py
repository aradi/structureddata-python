from structureddata.treewriter import TreeWriter

def test():
    'Test the treewriter module.'
    from structureddata.treereader import TreeReader
    reader = TreeReader()
    with open('H-H.msd', 'r') as fobj:
        tree = reader.read(fobj)
    writer = TreeWriter()
    with open('H-H.2.msd', 'w') as fobj:
        writer.write(fobj, tree)


if __name__ == '__main__':
    test()
