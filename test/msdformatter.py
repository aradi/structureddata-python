from structureddata.msdparser import MsdParser
from structureddata.msdformatter import MsdFormatter

def test():
    'Simple module test.'

    formatter = MsdFormatter()
    def open_block(name):
        'Customized block opening.'
        print(formatter.open_block(name), end='')

    def close_block(name):
        'Customized block closing.'
        print(formatter.close_block(name), end='')

    def add_data(name, data):
        'Customized data addition.'
        print(formatter.add_data(name, data), end='')

    parser = MsdParser()
    parser.open_block = open_block
    parser.close_block = close_block
    parser.add_data = add_data
    with open('H-H.msd', 'r') as fobj:
        parser.parse(fobj)


if __name__ == '__main__':
    test()
